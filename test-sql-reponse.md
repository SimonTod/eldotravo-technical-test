# Test technique (SQL) - Réponse

## Préparation

### STRUCTURE TABLES

```sql
CREATE DATABASE eldotravo_technical_test;

USE eldotravo_technical_test;

CREATE TABLE artisan (
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE user_review (
    id INT AUTO_INCREMENT PRIMARY KEY,
    artisan_id INT NOT NULL,
    author VARCHAR(255) NOT NULL,
    grade INT NOT NULL,
    comment VARCHAR(255) NOT NULL,
    review_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (artisan_id) REFERENCES artisan(id)
);
```

### POPULATION DES TABLES

```sql
USE eldotravo_technical_test;

INSERT INTO artisan (name) VALUES 
('SuperElec'),
('MegaPool'),
('GigaBat'),
('BestTuile'),
('SupraGarage');

INSERT INTO user_review (artisan_id, author, grade, comment, review_date) VALUES
(1, 'Paul', 4, 'Super electricien', '2020-05-02 15:02:11'),
(1, 'Jean', 5, 'Au Top !', '2020-05-03 15:02:11'),
(1, 'Charles', 2, 'Pourri à fond', '2020-05-04 15:02:11'),
(1, 'Xavier', 1, 'J\'ai même pas l\'électricité après son intervention', '2020-05-05 15:02:11'),
(1, 'Hubert', 4, 'RAS', '2020-05-06 15:02:11'),

(2, 'Paul', 4, 'Super electricien', '2020-05-02 15:02:11'),
(2, 'Jean', 5, 'Au Top !', '2020-05-03 15:02:11'),
(2, 'Charles', 2, 'Pourri à fond', '2020-05-04 15:02:11'),
(2, 'Xavier', 1, 'Ma piscine fui', '2020-05-05 15:02:11'),
(2, 'Hubert', 4, 'RAS', '2020-05-06 15:02:11'),

(3, 'Paul', 4, 'Super electricien', '2020-05-02 15:02:11'),
(3, 'Jean', 5, 'Au Top !', '2020-05-03 15:02:11'),
(3, 'Charles', 2, 'Pourri à fond', '2020-05-04 15:02:11'),
(3, 'Xavier', 1, 'J\'ai même pas de toit', '2020-05-05 15:02:11'),
(3, 'Hubert', 4, 'RAS', '2020-05-06 15:02:11'),

(4, 'Paul', 4, 'Super electricien', '2020-05-02 15:02:11'),
(4, 'Jean', 5, 'Au Top !', '2020-05-03 15:02:11'),
(4, 'Charles', 2, 'Pourri à fond', '2020-05-04 15:02:11'),
(4, 'Xavier', 1, '1 tuile sur 4 est manquante', '2020-05-05 15:02:11'),
(4, 'Hubert', 4, 'RAS', '2020-05-06 15:02:11'),

(5, 'Paul', 4, 'Super electricien', '2020-05-02 15:02:11'),
(5, 'Jean', 5, 'Au Top !', '2020-05-03 15:02:11'),
(5, 'Charles', 2, 'Pourri à fond', '2020-05-04 15:02:11'),
(5, 'Xavier', 1, 'Mon garage est en pente !', '2020-05-05 15:02:11'),
(5, 'Hubert', 4, 'RAS', '2020-05-06 15:02:11');
```

## SOLUTION

### REQUETE

```sql
SELECT t1.*
FROM user_review t1
WHERE t1.review_date >= (
	SELECT review_date FROM user_review t2
	WHERE t2.artisan_id = t1.artisan_id
    ORDER BY review_date DESC
    LIMIT 2,1
);
```

### EXPLICATIONS

La sous requête permet de récupérer, pour chaque artisan (`WHERE t2.artisan_id = t1.artisan_id`) 
la date de sa 3ème review la plus récente grâce à la fonction LIMIT (`LIMIT 2,1`).
Cette limite prend en premier argument l'offset, on démarre donc à l'offset 2, donc la 3ème occurrence,
et on limite à 1.

On récupère ensuite toutes les review qui ont une date plus récente que celle retrouvée par la sous requête.