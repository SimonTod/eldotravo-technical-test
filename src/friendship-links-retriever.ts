import { Person } from './person';

export class FriendshipLinksRetriever {
  private initialText: string;
  private affirmations: Array<string>;
  private question: string;
  private persons: Array<Person>;

  constructor(text: string) {
    this.persons = [];
    this.initialText = text;
    this.affirmations = this.getAffirmations();
    this.question = this.getQuestion();

    this.getPersonsAndFriendshipLinks();
  }

  public isFriend(): boolean {
    const questionSubject = this.getQuestionSubject();
    const myFriends = this.persons
      .find(person => person.name == Person.Moi)
      ?.findFriends();
    return myFriends ? myFriends.includes(questionSubject) : false;
  }

  /**
   * Gets affirmations sentences from initial text as an array of strings
   */
  private getAffirmations(): Array<string> {
    const affirmationsAsText = this.initialText.substr(
      0,
      this.initialText.indexOf('---'),
    );
    return affirmationsAsText
      .split('\n')
      .filter(affirmation => affirmation.length > 0);
  }

  /**
   * Gets question asked in initial text as a string
   */
  private getQuestion(): string {
    return this.initialText
      .substr(this.initialText.indexOf('---') + 3)
      .replace(/\n/g, '');
  }

  /**
   * Populate this.persons var with every subject from affirmations.
   * Also associates direct friends together.
   */
  private getPersonsAndFriendshipLinks(): void {
    this.affirmations.forEach(affirmation => {
      const subjects = this.getAffirmationsSubjects(affirmation);
      subjects.forEach(subject => {
        if (!this.persons.find(person => person.name == subject)) {
          this.persons.push(new Person(subject));
        }
      });
      const person1 = this.persons.find(person => person.name == subjects[0]);
      const person2 = this.persons.find(person => person.name == subjects[1]);
      if (person1 !== undefined && person2 !== undefined) {
        person1.addFriend(person2);
        person2.addFriend(person1);
      }
    });
  }

  /**
   * Get the two subjects from an affirmations and returns their names as an array (Person.Moi if the subject is me)
   *
   * @param string affirmation
   * @throws string
   */
  private getAffirmationsSubjects(affirmation: string): Array<string> {
    if (affirmation.match(/ est amie? avec moi/)) {
      const subject1 = affirmation.substr(0, affirmation.indexOf(' est ami'));
      const subject2 = Person.Moi;
      return [subject1, subject2];
    } else if (affirmation.match(/ est amie? avec /)) {
      const subject1 = affirmation.substr(0, affirmation.indexOf(' est ami'));
      const subject2 = affirmation.substr(affirmation.lastIndexOf(' ') + 1);
      return [subject1, subject2];
    } else if (affirmation.match(/Je suis ami avec /)) {
      const subject1 = Person.Moi;
      const subject2 = affirmation.substr(affirmation.lastIndexOf(' ') + 1);
      return [subject1, subject2];
    } else {
      throw "La syntaxe de l'affirmation n'a pas été reconnue : " + affirmation;
    }
  }

  /** Gets the subject from the question */
  private getQuestionSubject(): string {
    return this.question
      .replace('Est-ce que ', '')
      .replace(/ est mon amie? \?/, '');
  }
}
