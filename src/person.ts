export class Person {
  public static Moi = 'Moi';

  public name: string;
  public friends: Array<Person>;

  constructor(name: string) {
    this.name = name;
    this.friends = [];
  }

  public addFriend(person: Person): void {
    this.friends.push(person);
  }

  /**
   * Finds friends recursively.
   * Returns them in a array of string
   * @param alreadyListedFriends
   */
  public findFriends(alreadyListedFriends: Array<string> = []): Array<string> {
    const friends: Array<string> = alreadyListedFriends;
    this.friends.forEach(friend => {
      if (!friends.includes(friend.name) && friend.name !== Person.Moi) {
        friends.push(friend.name);
        friends.concat(friend.findFriends(friends));
      }
    });
    return friends;
  }
}
