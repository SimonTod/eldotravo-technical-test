import { FriendshipLinksRetriever } from './friendship-links-retriever';

const texts = [
  // TEXT 1
  'Benjamin est ami avec Paul\n' +
    'Sophie est amie avec moi\n' +
    'Je suis ami avec Benjamin\n' +
    '---\n' +
    'Est-ce que Sophie est mon amie ?',

  // TEXT 2
  'Benjamin est ami avec Paul\n' +
    'Frank est ami avec Paul\n' +
    'Mathieu est ami avec Aurore\n' +
    'Sophie est amie avec moi\n' +
    'Je suis ami avec Benjamin\n' +
    '---\n' +
    'Est-ce que Mathieu est mon ami ?',

  // TEXT 3
  'Benjamin est ami avec moi\n' +
    'Benjamin est ami avec Jean\n' +
    'Jean est ami avec Louise\n' +
    'Louise est amie avec Pierre\n' +
    '---\n' +
    'Est-ce que Pierre est mon ami ?',
];

texts.forEach((text, index) => {
  console.log('TEXT ' + (index + 1));
  console.log();
  console.log(text);
  const friendshipLinksRetriever = new FriendshipLinksRetriever(text);
  console.log(friendshipLinksRetriever.isFriend());
  console.log();
});
