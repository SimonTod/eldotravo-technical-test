# EldoTravo technical test

[![Open in Cloud Shell](https://user-images.githubusercontent.com/27065646/92304704-8d146d80-ef80-11ea-8c29-0deaabb1c702.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://gitlab.com/SimonTod/eldotravo-technical-test&tutorial=README.md)

### Scripts

#### `npm run start:dev`

Starts the application in development using `nodemon` and `ts-node` to do hot reloading.

#### `npm run build`

Builds the app at `build`, cleaning the folder first.

#### `npm run start`

Starts the app in production by first building the project with `npm run build`, and then executing the compiled JavaScript at `build/index.js`.

### Documentation

#### class Person

Représente une personne désigné dans le texte.
Permet de stocker son nom, ainsi que toutes ses affiliations (propriété `friends`).

#### class FriendshipLinksRetriever

C'est la classe qui est en charge de faire tout le processus.
Elle commence par séparer les affirmations en tableau de chaines (variable `affirmations`).
Puis elle sépare dans une autre variable (`question`) la question posée à la fin du texte.

Elle continue avec la méthode `getPersonsAndFriendshipLinks`.
Celle ci retrouve les différentes personnes mentionnées dans les affirmations, sans duplication, puis associe chacun des amis directs entre eux. (ami direct: 2 personnes mentionnées dans une affirmations seront amis direct).

L'initialisation est maintenant terminé. Lors de l'appel de la méthode `isFriend`, on recherche l'utilisateur courant (Moi), ainsi que le nom de la personne mentionnée dans la question.
On parcours de façon récursive la liste des amis en partant de l'utilisateur courant (Moi), afin de générer une liste de tous les amis (directs et indirects).
Dans cette liste d'amis, si le nom mentionné dans la question est présent, alors la question aura une réponse positive (`true`), sinon, la réponse sera négative (`false`).

#### fichier index.ts

Point d'entrée du code.

La variable `tests` stocke tous les textes qui seront testés.
On peut en ajouter autant que l'on souhaite.

Une boucle est ensuite faire sur ces textes, pour initialiser le test, et afficher la réponse dans la console.
